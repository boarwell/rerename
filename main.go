package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

const from = "problem.md"
const to = "practice.md"

func main() {
	root, err := os.Getwd()
	fmt.Println(root)
	if err != nil {
		log.Fatalln("error: failed to get current directory")
	}
	err = filepath.Walk(root, rerename)
	if err != nil {
		log.Fatalln(err)
	}

}

func rerename(path string, info os.FileInfo, err error) error {
	if info.Name() == from {
		fmt.Println(path)
		fmt.Println(filepath.Join(filepath.Dir(path), to))
		os.Rename(path, filepath.Join(filepath.Dir(path), to))
	}
	return nil
}
